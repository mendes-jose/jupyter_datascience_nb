from jupyter/datascience-notebook

RUN pip --no-cache-dir install joblib shapely descartes

ADD /.jupyter /home/jovyan/.jupyter/
